class Vector4f{
    /*
    Usage:
    On init, requires passing of 4 numbers, X, Y, Z, and the homogenous coordinate
    telling us whether it is a vector(direction) or a point in space.
    If h == 1, then it is a vector
    if h == 0, it is a point.

    let vector = new Vector4f(x, y, z, h)

     To call methods working with vectors, use

     let result = new Vector4f(coordinates).method(arguments);

     Example 1:
     let vector = new Vector4f(1, 1, 1, 1);
     let negatedVector = vector.negate(this);

     Example 2:
     let vector1 = new Vector4f(1, 1, 1, 1);
     let vector2 = new Vector4f(4, 5, 1, 1);
     let summed  = vector1.add(this, vector2);
     */
    constructor(x, y, z, h){
        this.x = x;
        this.y = y;
        this.z = z;
        this.h = h;
    }

    vector_out(htmlID){
        document.getElementById(htmlID).value += "v " + this.x.toString() + " " + this.y.toString()+ " "  + this.z.toString() + "\n";

    }

    negate(v){
        return new Vector4f(-v.x, -v.y, -v.z, v.h);
    }

    add(vector1, vector2){
        let x = vector1.x + vector2.x;
        let y = vector1.y + vector2.y;
        let z = vector1.z + vector2.z;
        let h = 0;
        if (vector1.h+vector2.h >= 1){
            h = 1;
        }
        return new Vector4f(x, y, z, h);
    }

    scalarProduct(v, s){
        return new Vector4f(s*v.x, s*v.y, s*v.z, v.h);
    }

    dotProduct(vector1, vector2){
        let x = vector1.x * vector2.x;
        let y = vector1.y * vector2.y;
        let z = vector1.z * vector2.z;
        return x+y+z;
    }

    crossProduct(v1, v2){
        let x = (v1.y*v2.z) - (v1.z*v2.y);
        let y = (v1.z*v2.x) - (v1.x*v2.z);
        let z = (v1.x*v2.y) - (v1.y*v2.x);
        alert("Vektorski produkt ni komutativna operacija, smer vektorja je odvisna od vrstnega reda operandov.");
        return new Vector4f(x, y, z, v1.h);
    }

    length(v){
        return Math.sqrt(v.x**2 + v.y**2 + v.z**2);
    }

    normalize(v){
        return this.scalarProduct(this.length(v), 1/len);
    }

    project(v1, v2){
        let adb = this.dotProduct(v1, v2);
        let blen = this.length(v2);
        let out = this.scalarProduct(adb, 1/blen);
        alert("Projekcija ni komutativna operacija. Projecirate prvi vektor na drugega.");
        return out;
    }

    cosPhi(v1, v2){
        let adb = this.dotProduct(v1, v2);
        let ablen = this.length(v1)*this.length(v2);
        let out = this.scalarProduct(adb, 1/ablen);
    }

}

class Matrix4f{
    /*
    Usage:
    On init, requires passing of an array of 16 numbers (can pass larger array,
    any member after index 15 will be ignored

    let matrix = new Matrix4f([a, b, c, d,
                               e, f, g, h,
                               i, j, k, l,
                               m, n, o, p])

     To call methods working on matrices, use

     let resultMatrix = Matrix4f.staticmethod(arguments);
     */
    constructor(array){
        this.matrix=[[0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]];
        var k=0;
        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                this.matrix[i][j]=array[k++];
            }
        }
    }

    getMatrixArray(){
        return this.matrix;
    }

    static getMatrixFrom2DArray(array){
        let newarr = [];
        let i = 0;
        for (line in array){
            for (element in line){
                newarr[k++] = element;
            }
        }
        return new Matrix4f(newarr);
    }


    static negate(m){
        let matrix = m.getMatrixArray();
        let outmatrix = [];
        let k=0;
        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                outmatrix[k++]=matrix[i][j]*(-1);
            }
        }
        return new Matrix4f(outmatrix);
    }

    static add(m1, m2){
        let mat1 = m1.getMatrixArray();
        let mat2 = m2.getMatrixArray();
        let outm = [];
        let k = 0;

        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                outm[k++] = mat1[i][j] + mat2[i][j];
            }
        }
        return new Matrix4f(outm);
    }

    static transpose(m){
        let mat = m.getMatrixArray();
        let outm= [];
        let k = 0;

        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                outm[k++]=mat[j][i];
            }
        }
        return new Matrix4f(outm);
    }

    static multiplyScalar(s, m){
        let mat = m.getMatrixArray();
        let outm = [];
        let k = 0;

        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                outm[k++] = mat[i][j]*s;
            }
        }
        return new Matrix4f(outm);
    }

    static multiply(m1, m2){
        let mat1 = m1.getMatrixArray();
        let mat2 = m2.getMatrixArray();
        let sum  = [[0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]];

        for (i=0; i<4; i++){
            for (j=0; j<4; j++){
                let s = 0;
                for (k=0; k<4; k++){
                    s += mat1[i][k]*mat2[k][j];
                }
                sum[i][j] = s;
            }
        }
        return this.getMatrixFrom2DArray(sum);

    }

    static multiplyArraysAsMatrices(m1, m2){
        let mat1 = m1;
        let mat2 = m2;
        let sum  = [[0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]];

        for (let i=0; i<4; i++){
            for (let j=0; j<4; j++){
                let s = 0;
                for (let k=0; k<4; k++){
                    s += mat1[i][k]*mat2[k][j];
                }
                sum[i][j] = s;
            }
        }
        return sum;
    }

    static mulwithvec(m1, v){
        let mat1 = m1.getMatrixArray();
        let vec = [[v.x], [v.y], [v.z], 1];
        let trv  = [0, 0, 0, 0];

        for (i=0; i<4; i++){ //for each line in matrix
            let sum = 0;
            for (j=0; j<4; j++){ //look at each element in line and vector
                //multiply mat1[i][j] with x[j] then sum up
                sum += mat1[i][j]*vec[j];
            }
            trv[i] = sum;
        }
        return new Vector4f(trv[0], trv[1], trv[2], trv[3]);
    }
}

class PointManager{

    constructor(pointless){
        this.dontuse = pointless;
        this.vectors = [];
    }

    getVectors(){
        let varr = [];
        let text = document.getElementById('input').value.split('\n');
        for (let i=0; i<text.length; i++){
            let coords = text[i].split(' ');
            //coords.length = 5;
            let vector = new Vector4f(coords[1], coords[2], coords[3], 1);
            varr.push(vector);
            this.vectors.push(vector);

        }
        return varr;

    }

    printVectors(array){
        document.getElementById("output").innerHTML = "";
        for (vector in array){
            vector.vector_out("output");
        }
        return 1;
    }
}

class Transformation{
    /*
    Usage:
    Say we have a Vector4f originalVector we want to transform,
    we would call this as:

    let transformedVector = Transformation
                            .[translate(vector)]
                            .[scale(vector)]
                            .[rotateXYZ(angles)]
                            .transformPoint(originalVector)

     where [SQUARE BRACKETS] denote optional transformations.
     */
    constructor() {
        this.tmatrix = [[0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0]];
    }

    static translate(v){
        let matrix = [[1, 0, 0,  v.x],
                       [0, 1, 0, v.y],
                       [0, 0, 1, v.z],
                       [0, 0, 0,  1 ]];
        this.tmatrix = Matrix4f.multiplyArraysAsMatrices(this.tmatrix, matrix);
    }

    static scale(v){
        let matrix = [[v.x,   0,    0, 0],
                       [  0, v.y,   0, 0],
                       [  0,   0, v.z, 0],
                       [  0,   0,   0, 1]];
        this.tmatrix = Matrix4f.multiplyArraysAsMatrices(this.tmatrix, matrix);
    }

    static rotateX(angle){
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let matrix = [[1,   0,     0, 0],
                       [0, cos, -sin, 0],
                       [0, sin,  cos, 0],
                       [0,   0,    0, 1]];
        this.tmatrix = Matrix4f.multiplyArraysAsMatrices(this.tmatrix, matrix);
    }

    static rotateY(angle){
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let matrix = [[ cos, 0,  sin, 0],
                       [   0, 1,   0, 0],
                       [-sin, 0, cos, 0],
                       [   0, 0,   0, 1]];
        this.tmatrix = Matrix4f.multiplyArraysAsMatrices(this.tmatrix, matrix);
    }

    static rotateZ(angle){
        let cos = Math.cos(angle);
        let sin = Math.sin(angle);
        let matrix = [[cos, -sin,  0, 0],
                       [sin,  cos, 0, 0],
                       [  0,    0, 1, 0],
                       [  0,    0, 0, 1]];
        this.tmatrix = Matrix4f.multiplyArraysAsMatrices(this.tmatrix, matrix);
    }

    static transformPoint(vector){
        return Matrix4f.mulwithvec(this.tmatrix, vector);
    }
}

class TransformPoints{

    static transform(varr){
        /*
        translacja vzdolž osi x za 1.25
        rotacija okoli osi z za kot pi/3
        translacija vzdolž osi z za 4.15
        translacija vzdolž osi y za 3.14
        skalacija vzdolž osi x in y za 1.12
        rotacija okoli osi y za kot 5*pi/8
         */

        let transarr = [];
        for (let vector in varr){
            let tvec = Transformation.translate(new Vector4f(1.25, 0, 0, 1))
                                     .rotateZ(Math.PI/3)
                                     .translate(new Vector4f(0, 0, 4.15, 1))
                                     .translate(new Vector4f(0, 3.14, 0, 1))
                                     .scale(new Vector4f(1.12, 1.12, 1, 1))
                                     .rotateY(5*(Math.PI/8)).transformPoint(vector);
            transarr.push(tvec);
        }
        return transarr;
    }
}